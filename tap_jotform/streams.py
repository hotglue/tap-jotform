"""Stream type classes for tap-jotform."""

from singer_sdk import typing as th  # JSON Schema typing helpers
from typing import Optional
from tap_jotform.client import JotFormStream


class FormsStream(JotFormStream):
    name = "forms"
    path = "/user/forms"
    primary_keys = ["id"]
    replication_key = "updated_at"
    records_jsonpath = "$.content[*]"
    schema = th.PropertiesList(
        th.Property("id", th.StringType),
        th.Property("username", th.StringType),
        th.Property("title", th.StringType),
        th.Property("id", th.StringType),
        th.Property("new", th.StringType),
        th.Property("count", th.StringType),
        th.Property("height", th.StringType),
        th.Property("status", th.StringType),
        th.Property("created_at", th.DateTimeType),
        th.Property("updated_at", th.DateTimeType),
        th.Property("last_submission", th.StringType),
        th.Property("type", th.StringType),
        th.Property("favorite", th.StringType),
        th.Property("archived", th.StringType),
        th.Property("url", th.StringType),
    ).to_dict()

    def get_child_context(self, record: dict, context: Optional[dict]) -> dict:
        return {
            "form_id": record["id"]
        }


class FormSubmissionsStream(JotFormStream):
    name = "form_submissions"
    records_jsonpath = "$.content[*]"
    primary_keys = ["id"]
    replication_key = "created_at"
    parent_stream_type = FormsStream
    path = "/form/{form_id}/submissions"

    schema = th.PropertiesList(
        th.Property("id", th.StringType),
        th.Property("ip", th.StringType),
        th.Property("created_at", th.DateTimeType),
        th.Property("status", th.DateTimeType),
        th.Property("new", th.StringType),
        th.Property("flag", th.StringType),
        th.Property("notes", th.StringType),
        th.Property("updated_at", th.DateTimeType),
        th.Property("form_id", th.StringType),
        th.Property("answers", th.CustomType({"type": ["object", "string"]})),
    ).to_dict()
