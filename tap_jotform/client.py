"""REST client handling, including JotFormStream base class."""

import requests
from typing import Any, Dict, Optional, Iterable

from memoization import cached

from singer_sdk.helpers.jsonpath import extract_jsonpath
from singer_sdk.streams import RESTStream
from singer_sdk.authenticators import APIKeyAuthenticator



class JotFormStream(RESTStream):
    """JotForm stream class."""

    @property
    def url_base(self) -> str:
        sub_domain = self.config.get("subdomain")
        if sub_domain:
            return f"https://{sub_domain}.jotform.com"
        else:
            return "https://api.jotform.com"

    records_jsonpath = "$[*]"  # Or override `parse_response`.
    _page_size = 1000

    @property
    def authenticator(self) -> APIKeyAuthenticator:
        """Return a new authenticator object."""
        return APIKeyAuthenticator.create_for_stream(
            self, key="APIKEY", value=self.config.get("api_key"), location="header"
        )

    def get_next_page_token(
        self, response: requests.Response, previous_token: Optional[Any]
    ) -> Optional[Any]:
        """Return a token for identifying next page or None if no more pages."""

        if not previous_token:
            previous_token = 0

        response_json = response.json()
        limit = response_json["resultSet"]["limit"]
        count = response_json["resultSet"]["count"]

        if count < limit:
            return None
        else:
            return previous_token + self._page_size

    def get_url_params(
        self, context: Optional[dict], next_page_token: Optional[Any]
    ) -> Dict[str, Any]:
        """Return a dictionary of values to be used in URL parameterization."""
        params: dict = {}
        params["apiKey"] = self.config.get("api_key")
        params["offset"] = f"{(next_page_token or 0)}"
        params["limit"] = self._page_size

        if self.replication_key:
            params["orderBy"] = f"{self.replication_key}"
            params["date"] = 1

            start_date = self.get_starting_timestamp(context)
            if start_date is not None:
                date = start_date.strftime("%Y-%m-%d %H:%M:%S")
                # the filter parameter is a json string
                params[f"filter"] = '{"created_at:gt":' + '"' + date + '"' + "}"

        return params

    def prepare_request_payload(
        self, context: Optional[dict], next_page_token: Optional[Any]
    ) -> Optional[dict]:
        """Prepare the data payload for the REST API request.

        By default, no payload will be sent (return None).
        """
        # TODO: Delete this method if no payload is required. (Most REST APIs.)
        return None

    def parse_response(self, response: requests.Response) -> Iterable[dict]:
        """Parse the response and return an iterator of result records."""
        # TODO: Parse response body and return a set of records.
        yield from extract_jsonpath(self.records_jsonpath, input=response.json())

    def post_process(self, row: dict, context: Optional[dict]) -> dict:
        """As needed, append or transform raw data to match expected structure."""
        # TODO: Delete this method if not needed.
        return row
